# model.py

import sqlite3
from database import conn, cursor


def check_connection():
    global conn, cursor
    if conn is None or cursor is None:
        conn = sqlite3.connect('shopping_list.db', check_same_thread=False)
        cursor = conn.cursor()

def close_connection():
    global conn, cursor
    if conn:
        cursor.close()
        conn.close()
        conn = None
        cursor = None

def get_last_message_id(user_id):
    check_connection()
    cursor.execute('SELECT last_message_id FROM user WHERE user_id = ? LIMIT 1', (user_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def get_list_name(list_id):
    check_connection()
    cursor.execute('SELECT name FROM list WHERE list_id = ? LIMIT 1', (list_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def get_element_name(element_id):
    check_connection()
    cursor.execute('SELECT name from element WHERE element_id = ? LIMIT 1', (element_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def get_user_last_message_by_list(list_id):
    check_connection()
    cursor.execute('SELECT ul.user_id, u.last_message_id FROM user_list ul JOIN user u ON u.user_id = ul.user_id WHERE ul.is_active = 1 AND ul.list_id = ?', (list_id,))
    return cursor.fetchall()

def get_list_elements(list_id):
    check_connection()
    cursor.execute('SELECT element_id, name, bought FROM element WHERE list_id = ? ORDER BY name ASC', (list_id,))
    return cursor.fetchall()

def get_list_element_count(list_id):
    check_connection()
    cursor.execute('SELECT count(element_id) FROM element WHERE list_id = ?', (list_id,))
    return cursor.fetchall()

def switch_bought_element(element_id):
    check_connection()
    cursor.execute('SELECT bought FROM element WHERE element_id = ?', (element_id,))
    row = cursor.fetchone()
    if row:
        bought = row[0]
        cursor.execute('UPDATE element SET bought = ? WHERE element_id = ?', (not bought, element_id,))
        conn.commit()

def update_element_name(name, element_id):
    check_connection()
    cursor.execute('UPDATE element SET name = ? WHERE element_id = ?', (name, element_id,))
    conn.commit()

def delete_element(element_id):
    check_connection()
    cursor.execute('DELETE FROM element WHERE element_id = ?', (element_id,))
    conn.commit()

def create_element(name, list_id):
    check_connection()
    cursor.execute('INSERT INTO element (list_id, name, bought) VALUES (?, ?, 0)', (list_id, name, ))
    conn.commit()

def get_user_lists(user_id):
    check_connection()
    cursor.execute('SELECT l.list_id, l.name, ul.is_active FROM list l INNER JOIN user_list ul ON ul.list_id = l.list_id WHERE ul.user_id = ? ORDER BY l.name ASC', (user_id, ))
    return cursor.fetchall()

def get_user_list_by_list_id(list_id):
    check_connection()
    cursor.execute('SELECT user_id FROM user_list WHERE list_id = ?', (list_id, ))
    return cursor.fetchall()

def delete_from_user_list(user_id, list_id):
    check_connection()
    cursor.execute('DELETE FROM user_list WHERE list_id = ? AND user_id = ?', (list_id, user_id, ))
    conn.commit()
    user_list = get_user_list_by_list_id(list_id)
    new_owner_id = user_list[0][0] if user_list else None
    if new_owner_id:
        cursor.execute('UPDATE list SET owner_id = ? WHERE list_id = ?', (new_owner_id, list_id, ))
    else:
        cursor.execute('DELETE FROM list WHERE list_id = ?', (list_id, ))
        cursor.execute('DELETE FROM element WHERE list_id = ?', (list_id, ))
    cursor.execute('DELETE FROM invite_list WHERE user_id = ? AND list_id = ? ', (user_id, list_id, ))
    conn.commit()

def switch_owner(user_id, list_id, previous_owner_id):
    check_connection()
    cursor.execute("UPDATE list SET owner_id = ? WHERE list_id = ?", (user_id, list_id, ))
    cursor.execute("DELETE FROM invite_list WHERE user_id = ? AND list_id = ?", (previous_owner_id, list_id, ))
    conn.commit()

def update_list_name(name, list_id):
    check_connection()
    cursor.execute('UPDATE list SET name = ? WHERE list_id = ?', (name, list_id,))
    conn.commit()

def create_list(name, user_id):
    check_connection()
    cursor.execute('INSERT OR IGNORE INTO list (name, owner_id) VALUES (?, ?)', (name, user_id, ))
    conn.commit()
    list_id = cursor.lastrowid
    # disable all
    cursor.execute('UPDATE user_list SET is_active = 0 WHERE user_id = ?', (user_id, ))
    # enable new list
    cursor.execute('INSERT INTO user_list (list_id, user_id, is_active) VALUES (?, ?, 1)', (list_id, user_id, ))
    conn.commit()
    return list_id

def set_last_message_id(message_id, user_id):
    check_connection()
    cursor.execute('UPDATE user SET last_message_id = ? WHERE user_id = ?', (message_id, user_id,))
    conn.commit()

def is_user_id_exists(user_id):
    check_connection()
    cursor.execute('SELECT user_id FROM user WHERE user_id = ? LIMIT 1', (user_id,))
    row = cursor.fetchone()
    return True if row else False

def set_new_user(user_id):
    check_connection()
    cursor.execute('INSERT INTO user (user_id) VALUES (?)', (user_id,))
    conn.commit()

def get_invite_token(list_id, user_id):
    check_connection()
    cursor.execute('SELECT token FROM invite_list WHERE list_id = ? AND user_id = ? LIMIT 1', (list_id, user_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def set_invite_token(list_id, user_id, invite_token):
    check_connection()
    cursor.execute('INSERT INTO invite_list (list_id, user_id, token) VALUES (?, ?, ?)', (list_id, user_id, invite_token, ))
    conn.commit()

def set_user_into_list(list_id, user_id):
    check_connection()
    cursor.execute('INSERT OR IGNORE INTO user_list (list_id, user_id) VALUES (?, ?)', (list_id, user_id,))
    conn.commit()

def get_list_id_by_invite(token):
    check_connection()
    cursor.execute('SELECT list_id FROM invite_list WHERE token = ? LIMIT 1', (token,))
    row = cursor.fetchone()
    return row[0] if row else None

def get_active_list(user_id):
    check_connection()
    cursor.execute('SELECT list_id FROM user_list WHERE user_id = ? ORDER BY is_active DESC LIMIT 1', (user_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def get_list_id_if_owner(user_id):
    check_connection()
    cursor.execute('''
        SELECT ul.list_id
        FROM user_list ul
        LEFT JOIN list l ON l.list_id = ul.list_id AND l.owner_id = ul.user_id
        WHERE l.owner_id = ? AND ul.is_active = 1
        LIMIT 1
    ''', (user_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def get_owner_id(list_id):
    check_connection()
    cursor.execute('SELECT owner_id FROM list WHERE list_id = ?', (list_id,))
    row = cursor.fetchone()
    return row[0] if row else None

def check_is_owner(user_id, list_id):
    check_connection()
    cursor.execute('''
        SELECT TRUE
        FROM user_list ul
        LEFT JOIN list l ON l.list_id = ul.list_id AND l.owner_id = ul.user_id
        WHERE l.owner_id = ? AND ul.is_active = 1 AND ul.list_id = ?
        LIMIT 1
    ''', (user_id, list_id))
    row = cursor.fetchone()
    return row[0] if row else None

def get_user_language(user_id):
    check_connection()
    cursor.execute("SELECT language FROM user WHERE user_id = ?", (user_id,))
    result = cursor.fetchone()
    if result:
        return result[0]
    return None

def set_user_language(user_id, language):
    check_connection()
    cursor.execute("SELECT user_id FROM user WHERE user_id = ? LIMIT 1", (user_id,))
    row = cursor.fetchone()
    
    if row:
        cursor.execute("UPDATE user SET language = ? WHERE user_id = ?", (language, user_id,))
        conn.commit()
    else:
        cursor.execute("INSERT INTO user (user_id, language) VALUES (?, ?)", (user_id, language,))
        conn.commit()

def switch_select_list(user_id, list_id):
    check_connection()
    if list_id != get_active_list(user_id):
        cursor.execute('UPDATE user_list SET is_active = 0 WHERE user_id = ?', (user_id,))
        cursor.execute('UPDATE user_list SET is_active = 1 WHERE user_id = ? AND list_id = ?', (user_id, list_id,))
        conn.commit()
