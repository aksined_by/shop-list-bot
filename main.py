# main.py

import logging
from config import TOKEN
from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, CallbackQueryHandler, filters
from handlers import button, start, text


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

def main():
    application = ApplicationBuilder().token(TOKEN).build()
    
    application.add_handler(CommandHandler('start', start.handler))
    application.add_handler(CallbackQueryHandler(button.handler))
    application.add_handler(MessageHandler(filters.ALL, text.handler))

    application.run_polling()

if __name__ == '__main__':
    main()
