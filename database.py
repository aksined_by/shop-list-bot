# database.py

import sqlite3

# Database setup
conn = sqlite3.connect('shopping_list.db', check_same_thread=False)
cursor = conn.cursor()

cursor.execute('''
CREATE TABLE IF NOT EXISTS user (
    user_id INTEGER PRIMARY KEY,
    last_message_id INTEGER,
	language TEXT DEFAULT 'en',
	is_premium INTEGER DEFAULT 0
)
''')

cursor.execute('''
CREATE TABLE IF NOT EXISTS list (
    list_id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    owner_id INTEGER,
    FOREIGN KEY(owner_id) REFERENCES user(user_id) ON DELETE CASCADE
)
''')

cursor.execute('''
CREATE TABLE IF NOT EXISTS element (
    element_id INTEGER PRIMARY KEY AUTOINCREMENT,
    list_id INTEGER,
    name TEXT,
    bought BOOLEAN,
    FOREIGN KEY(list_id) REFERENCES list(list_id) ON DELETE CASCADE
)
''')

cursor.execute('''
CREATE TABLE IF NOT EXISTS user_list (
    list_id INTEGER,
    user_id INTEGER,
    is_active BOOLEAN,
    UNIQUE (list_id, user_id),
    FOREIGN KEY(list_id) REFERENCES list(list_id) ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES user(user_id) ON DELETE CASCADE
)
''')

cursor.execute('''
CREATE TABLE IF NOT EXISTS invite_list (
    list_id INTEGER,
    user_id INTEGER,
    token VARCHAR(32),
    UNIQUE (list_id, user_id),
    FOREIGN KEY(list_id) REFERENCES list(list_id) ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES user(user_id) ON DELETE CASCADE
)
''')

cursor.execute('CREATE INDEX IF NOT EXISTS idx_token ON invite_list (token)')

conn.commit()
