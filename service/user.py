# service/user.py

from model import is_user_id_exists, set_new_user


def create_user_if_not_exists(user_id):
    is_user_exists = is_user_id_exists(user_id)
    
    if not is_user_exists:
        set_new_user(user_id)
