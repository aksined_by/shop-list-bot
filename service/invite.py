# service/invite.py

from config import TOKEN, BOT_NAME
from model import get_invite_token, set_invite_token
import hashlib
import random
import string


def generate_invite_token(user_id, list_id):
    hash_object = hashlib.sha256(f"{user_id}{list_id}{TOKEN}".encode())
    hash_hex = hash_object.hexdigest()

    if len(hash_hex) > 32:
        final_token = hash_hex[:32]
    else:
        random_padding = ''.join(random.choices(string.ascii_letters + string.digits, k=32-len(hash_hex)))
        final_token = hash_hex + random_padding
    
    return final_token

def generate_invite_link(user_id, list_id):
    invite_token = get_invite_token(list_id, user_id)

    if not invite_token:
        invite_token = generate_invite_token(user_id, list_id)
        set_invite_token(list_id, user_id, invite_token)

    return f"https://t.me/{BOT_NAME}?start=invite_{invite_token}"
