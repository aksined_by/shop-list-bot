# service/start.py

from model import get_list_id_by_invite, set_user_into_list
from service import list


def handle_start_action(text, user_id):
    if text.startswith("/start invite_"):
        list_token = text.split('_')[1]
        list_id = get_list_id_by_invite(list_token)

        if list_id:
            set_user_into_list(list_id, user_id)
            return list_id

    return list.create_default_list_if_not_exists(user_id)
