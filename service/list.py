# service/list.py

from model import get_active_list, get_user_last_message_by_list, get_list_name, set_last_message_id, close_connection, create_list
from helpers.language import translate
from keyboards import build_member_list, build_list_keyboard
import re


def create_default_list_if_not_exists(user_id):
    list_id = get_active_list(user_id)

    if not list_id:
        list_id = create_list("Default list", user_id)
        
    return list_id

def rename_list_action_parser(string):
    pattern = r"rename_list_(\d+)(?:_(.*))?"
    match = re.match(pattern, string)
    if match:
        number = match.group(1)
        action = match.group(2) if match.group(2) else "manage_lists"
        return number, action
    return None, "manage_lists"

def invite_list_action_parser(string):
    pattern = r"invite_(\d+)(?:_(.*))?"
    match = re.match(pattern, string)
    if match:
        number = match.group(1)
        action = match.group(2) if match.group(2) else "menu"
        return number, action
    return None, "menu"

def manage_list_action_parser(string):
    pattern = r"manage_lists(?:_(.*))?"
    match = re.match(pattern, string)
    if match:
        return match.group(1) if match.group(1) else "menu"
    return "menu"

async def render_member_list(context, query, user_id):
    list_id = get_active_list(user_id)
    keyboard = await build_member_list(context, list_id, user_id)
    await query.edit_message_text(text=translate("show_members"), reply_markup=keyboard)

async def update_list_messages(context, list_id) -> None:
    context.user_data['action'] = None

    for user_id, last_message_id in get_user_last_message_by_list(list_id):
        try:
            await context.bot.edit_message_text(
                chat_id=user_id,
                message_id=last_message_id,
                text=translate("start_message", LIST=get_list_name(list_id)),
                reply_markup=build_list_keyboard(user_id, list_id),
                parse_mode='HTML'
            )
        except Exception:
            message = await context.bot.send_message(
                chat_id=user_id,
                text=translate("start_message", LIST=get_list_name(list_id)),
                reply_markup=build_list_keyboard(user_id, list_id),
                parse_mode='HTML'
            )
            set_last_message_id(message.message_id, user_id)
    close_connection()
