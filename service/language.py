# service/language.py

from model import get_user_language, set_user_language
from helpers.language import  set_language, get_lang_list_short


def check_is_language_installed(user):
    if '_' in globals():
        return

    user_language = get_user_language(user.id)
    
    if user_language is not None and user_language != 0:
        set_language(user_language)
    else:
        user_lang_code = user.language_code
        if user_lang_code not in get_lang_list_short():
            user_lang_code = 'en'
        set_language(user_lang_code)
        set_user_language(user.id, user_lang_code)
