# keyboards.py

from helpers.language import translate, get_lang_list
from model import get_user_list_by_list_id, get_owner_id, switch_select_list, get_list_elements, get_user_lists
from telegram import error, InlineKeyboardMarkup, InlineKeyboardButton


def build_list_keyboard(user_id, list_id):
    switch_select_list(user_id, list_id)
    items = get_list_elements(list_id)
    keyboard = []

    if items:
        for item in items:
            status = "✅" if item[2] else "🛒"
            keyboard.append([
                InlineKeyboardButton(f"{item[1]}", callback_data=f"rename_element_{item[0]}"),
                InlineKeyboardButton(f"{status}", callback_data=f"toggle_element_bought_{item[0]}"),
                InlineKeyboardButton("🗑", callback_data=f"delete_element_{item[0]}")
            ])
    else:
        keyboard.append([InlineKeyboardButton(translate("empty_list"), callback_data="add_element")])

    keyboard.append([
        InlineKeyboardButton(translate("action_menu"), callback_data="menu"),
        InlineKeyboardButton(translate("action_menu_list"), callback_data=f"menu_list")
    ])

    return InlineKeyboardMarkup(keyboard)

def build_menu_keyboard():
    return InlineKeyboardMarkup([
        [InlineKeyboardButton(translate("action_manage_list"), callback_data="manage_lists")],
        [InlineKeyboardButton(translate("action_language"), callback_data="language_list")],
        [InlineKeyboardButton(translate("action_back_to_list"), callback_data="show_list")]
    ])

def build_list_menu_keyboard(list_id):
    return InlineKeyboardMarkup([
        [InlineKeyboardButton(translate("action_add_element"), callback_data="add_element")],
        [InlineKeyboardButton(translate("action_show_members"), callback_data="show_members")],
        [InlineKeyboardButton(translate("action_switch_list"), callback_data="manage_lists_menu_list")],
        [InlineKeyboardButton(translate("action_rename_list"), callback_data=f"rename_list_{list_id}_menu_list")],
        [InlineKeyboardButton(translate("action_invite"), callback_data=f"invite_{list_id}_menu_list")],
        [InlineKeyboardButton(translate("action_back_to_list"), callback_data="show_list")]
    ])

def build_manage_lists_keyboard(user_id, back_action = "menu"):
    keyboard = []
    lists = get_user_lists(user_id)
    for list in lists:
        row = [
            InlineKeyboardButton(list[1], callback_data=f"rename_list_{list[0]}"),
            InlineKeyboardButton(translate("action_select"), callback_data=f"select_list_{list[0]}")
        ]

        if back_action == "menu":
            if list[2]:
                row.append(InlineKeyboardButton(translate("action_active_list"), callback_data=f"none"))
            else:
                row.append(InlineKeyboardButton(translate("action_delete_list"), callback_data=f"delete_list_{list[0]}"))

        keyboard.append(row)
        
    if back_action == "menu":
        keyboard.append([InlineKeyboardButton(translate("action_create_new_list"), callback_data="create_list")])

    keyboard.append([InlineKeyboardButton(translate("back"), callback_data=back_action)])
    return InlineKeyboardMarkup(keyboard)

def build_language_list():
    keyboard = []

    for lang_code, lang_name in get_lang_list():
        keyboard.append([InlineKeyboardButton(lang_name, callback_data=f"switch_lang_{lang_code}")])

    keyboard.append([get_button_back_to_main_menu()])

    return InlineKeyboardMarkup(keyboard)

def get_button_back_to_main_menu():
    return InlineKeyboardButton(translate("back"), callback_data="menu")

def build_back_to_list_menu():
    return InlineKeyboardMarkup([[InlineKeyboardButton(translate("back"), callback_data="menu_list")]])

def build_back_to_manage_list():
    return InlineKeyboardMarkup([[InlineKeyboardButton(translate("back"), callback_data="manage_lists")]])

def build_back_to_list():
    return InlineKeyboardMarkup([[InlineKeyboardButton(translate("back"), callback_data="show_list")]])

async def build_member_list(context, list_id, user_id):
    keyboard = []
    owner_id = get_owner_id(list_id)
    is_allow_handle = owner_id == user_id
    member_list = get_user_list_by_list_id(list_id)

    for member_row in member_list:
        try:
            user = await context.bot.get_chat(member_row[0])
            username = user.username
        except error.BadRequest as e:
            print(f"Chat with user_id {member_row[0]} not found.")
            username = "Unknown"

        if member_row[0] == owner_id:
            keyboard.append([InlineKeyboardButton(f"@{username} (owner)", url=f"tg://resolve?domain={username}")])
        else:
            member_keyboard_row = [InlineKeyboardButton(f"@{username}", url=f"tg://resolve?domain={username}")]
            if is_allow_handle:
                member_keyboard_row.append(InlineKeyboardButton("🗑", callback_data=f"remove_user_from_list_{member_row[0]}_{list_id}"))
                member_keyboard_row.append(InlineKeyboardButton(translate("action_set_owner"), callback_data=f"set_owner_{member_row[0]}_{list_id}"))
            
            keyboard.append(member_keyboard_row)

    keyboard.append([InlineKeyboardButton(translate("back"), callback_data="menu_list")])

    return InlineKeyboardMarkup(keyboard)
