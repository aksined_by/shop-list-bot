import sqlite3


def view_table(table_name):
    conn = sqlite3.connect('shopping_list.db')
    cursor = conn.cursor()
    cursor.execute(f"SELECT * FROM {table_name}")
    rows = cursor.fetchall()
    conn.close()
    for row in rows:
        print(row)

# conn = sqlite3.connect('shopping_list.db')
# cursor = conn.cursor()
# cursor.execute("DELETE FROM user_groups WHERE user_id = 345519744")
# conn.commit()
# conn.close()
    
# Примеры использования
view_table('list')
view_table('user')
view_table('user_list')
view_table('element')

