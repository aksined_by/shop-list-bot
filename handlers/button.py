# handlers/button.py

from helpers.language import set_language, translate
from service.list import *
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ContextTypes
from keyboards import build_language_list, build_list_menu_keyboard, build_list_keyboard, build_menu_keyboard, build_manage_lists_keyboard, build_back_to_list_menu, build_back_to_manage_list, build_back_to_list
from model import get_active_list, get_list_name, set_last_message_id, set_user_language, check_is_owner, switch_bought_element, get_element_name, delete_element, delete_from_user_list, switch_owner, close_connection
from service import invite, language


async def handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    context.user_data['action'] = None
    language.check_is_language_installed(update.effective_user)
    query = update.callback_query
    user_id = update.effective_user.id
    await query.answer()
    
    data = query.data[:64]
    
    if data == "menu":
        await query.edit_message_text(text=translate("menu"), reply_markup=build_menu_keyboard())
    elif data == "menu_list":
        list_id = get_active_list(user_id)
        await query.edit_message_text(text=translate("action_menu_list"), reply_markup=build_list_menu_keyboard(list_id))
    elif data == "add_element":
        await query.edit_message_text(text=translate("add_element"), reply_markup=build_back_to_list_menu())
    elif data == "create_list":
        await query.edit_message_text(text=translate("create_list"), reply_markup=build_back_to_manage_list())
        context.user_data['action'] = "create_list"
    elif data == "show_members":
        await render_member_list(context, query, user_id)
    elif data == "show_list":
        list_id = get_active_list(user_id)
        await query.edit_message_text(
            text=translate("start_message", LIST=get_list_name(list_id)),
            reply_markup=build_list_keyboard(user_id, list_id),
            parse_mode='HTML'
        )
        set_last_message_id(query.message.message_id, user_id)
    elif data == "language_list":
        await query.edit_message_text(text=translate("action_language"), reply_markup=build_language_list())
    elif data.startswith("switch_lang_"):
        lang_code = data.split('_')[2]
        set_language(lang_code)
        set_user_language(user_id, lang_code)
        list_id = get_active_list(user_id)
        await update_list_messages(context, list_id)
    elif data.startswith("manage_lists"):
        back_action = manage_list_action_parser(data)
        text = "manage_lists" if back_action == "menu" else "switch_lists"
        await query.edit_message_text(text=translate(text), reply_markup=build_manage_lists_keyboard(user_id, back_action=back_action))
    elif data.startswith("invite_"):
        list_id, callback = invite_list_action_parser(data)
        is_owner = check_is_owner(update.effective_user.id, list_id)
        if is_owner:
            link = invite.generate_invite_link(update.effective_user.id, list_id)
            await query.edit_message_text(
                translate("invite", LINK=link),
                reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(translate("back"), callback_data=callback)]]),
                parse_mode='HTML'
            )
        else:
            await query.edit_message_text(translate("ivite_denied"), reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(translate("back"), callback_data=callback)]]))
    elif data.startswith("toggle_element_bought_"):
        item_id = int(data.split('_')[3])
        switch_bought_element(item_id)
        list_id = get_active_list(user_id)
        await update_list_messages(context, list_id)
    elif data.startswith("rename_element_"):
        item_id = int(data.split('_')[2])
        await query.edit_message_text(
            text=translate("rename_element", OLD_NAME=get_element_name(item_id)),
            reply_markup=build_back_to_list(),
            parse_mode='HTML'
        )
        context.user_data['action'] = f"rename_element_{item_id}"
    elif data.startswith("delete_element_"):
        item_id = int(data.split('_')[2])
        delete_element(item_id)
        list_id = get_active_list(user_id)
        await update_list_messages(context, list_id)
    elif data.startswith("select_list_"):
        list_id = data.split('_')[2]
        list_name = get_list_name(list_id)
        await query.edit_message_text(
            text=translate("start_message", LIST=list_name),
            reply_markup=build_list_keyboard(user_id, list_id),
            parse_mode='HTML'
        )
    elif data.startswith("rename_list_"):
        list_id, callback = rename_list_action_parser(data)
        await query.edit_message_text(
            text=translate("rename_list", OLD_NAME=get_list_name(list_id)),
            reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(translate("back"), callback_data=callback)]]),
            parse_mode='HTML'
        )
        context.user_data['action'] = f"rename_list_{list_id}"
    elif data.startswith("delete_list_"):
        list_id = int(data.split('_')[2])
        delete_from_user_list(user_id, list_id)
        await query.edit_message_text(text=translate("switch_lists"), reply_markup=build_manage_lists_keyboard(user_id))
    elif data.startswith("remove_user_from_list_"):
        parsed_command = data.split('_')
        removed_user_id = int(parsed_command[4])
        delete_from_user_list(removed_user_id, int(parsed_command[5]))
        create_default_list_if_not_exists(removed_user_id)
        await render_member_list(context, query, user_id)
    elif data.startswith("set_owner_"):
        parsed_command = data.split('_')
        switch_owner(int(parsed_command[2]), int(parsed_command[3]), user_id)
        await render_member_list(context, query, user_id)
    close_connection()
