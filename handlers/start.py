# handlers/start.py

from helpers.language import translate
from service import language, start, user
from telegram import Update
from telegram.ext import ContextTypes
from keyboards import build_list_keyboard
from model import get_list_name, set_last_message_id, close_connection


async def handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    context.user_data['action'] = None
    user.create_user_if_not_exists(update.effective_user.id)
    language.check_is_language_installed(update.effective_user)

    list_id = start.handle_start_action(update.message.text, update.effective_user.id)

    message = await context.bot.send_message(
        chat_id=update.effective_chat.id, 
        text=translate("start_message", LIST=get_list_name(list_id)),
        reply_markup=build_list_keyboard(update.effective_user.id, list_id),
        parse_mode='HTML'
    )
    set_last_message_id(message.message_id, update.effective_user.id)
    close_connection()
