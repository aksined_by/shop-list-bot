# handlers/text.py

from service import language, list
from telegram import Update
from telegram.ext import ContextTypes
from model import get_active_list, update_element_name, update_list_name, create_list, create_element, close_connection


async def handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if not update.message.text:
        await update.message.delete()
        return

    language.check_is_language_installed(update.effective_user)
    list_id = get_active_list(update.effective_user.id)
    action = context.user_data.get('action')
    user_text = update.message.text.replace('\n', ' ')
    
    if action and action.startswith("rename_element_"):
        update_element_name(user_text, int(action.split('_')[2]))
    elif action and action.startswith("rename_list_"):
        update_list_name(user_text, int(action.split('_')[2]))
    elif action == "create_list":
        list_id = create_list(user_text, update.effective_user.id)
    else:
        create_element(user_text, list_id)

    context.user_data['action'] = None
    await update.message.delete()
    await list.update_list_messages(context, list_id)
    close_connection()
