# language.py

import gettext


en = gettext.translation('bot', localedir='locales', languages=['en'])
ru = gettext.translation('bot', localedir='locales', languages=['ru'])
pl = gettext.translation('bot', localedir='locales', languages=['pl'])

def set_language(lang):
    global _
    if lang == 'en':
        en.install()
    elif lang == 'ru':
        ru.install()
    elif lang == 'pl':
        pl.install()

def translate(text, **kwargs):
    translation = _(text)
    for key, value in kwargs.items():
        translation = translation.replace(f"{{{{{key}}}}}", value)
    return translation

def get_lang_list():
    return [("en", "English"), ("pl", "Polski"), ("ru", "Русский")]

def get_lang_list_short():
    return ["en", "pl", "ru"]
